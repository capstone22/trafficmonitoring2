import React from 'react';
import {PartnerData} from  './PartnerData.js';
import Chart from './Chart.js';

// this fetches the data, later it will be used to graph a line graph with axises time vs #of transactions
// time will be hourly, thus will only use the first value in the timestamp, ex. 4:35 will only use 4
// hour will be used to plot x-axis location on the graph
export default class DataLoader extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        error: null,
        isLoaded: false,
        items: PartnerData
      };
    }
  
    render() {
      const {items } = this.state;

        return (<Chart rawData={items}/>);
    }
  }